<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link    https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */
require_once( __DIR__ . '/vendor/autoload.php' );
$dotenv = Dotenv\Dotenv::createImmutable( __DIR__ );
$dotenv->load();

/** Define vars */
$local_envs       = [ 'local', 'staging', 'dev' ];
$debug            = ( isset( $_SERVER['APP_ENV'] ) && in_array( $_SERVER['APP_ENV'], $local_envs ) ) || ( isset( $_ENV['ENV'] ) && in_array( $_ENV['ENV'], $local_envs ) );
$auto_update_core = ! empty( $_ENV['WP_AUTO_UPDATE_CORE'] ) ? $_ENV['WP_AUTO_UPDATE_CORE'] : 'minor';

/**
* Define WordPress environment type.
*
* @see https://make.wordpress.org/core/2020/07/24/new-wp_get_environment_type-function-in-wordpress-5-5/
*/
define( 'WP_ENVIRONMENT_TYPE', $_ENV['ENV'] );

/** Auto updating WordPress core */
define( 'WP_AUTO_UPDATE_CORE', $auto_update_core );

/** Caching */
define( 'WP_CACHE', $_ENV['WP_CACHE'] ); // @see https://docs.wp-rocket.me/article/958-how-to-use-wp-rocket-with-bedrock-wordpress-boilerplate

/** File mods */
define( 'DISALLOW_FILE_EDIT', ! $debug );
define( 'DISALLOW_FILE_MODS', ! $debug );
define( 'DISABLE_NAG_NOTICES', ! $debug );

/** Misc **/
define( 'WP_ROCKET_WHITE_LABEL_ACCOUNT', true );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', $_ENV['DB_NAME'] );

/** MySQL database username */
define( 'DB_USER', $_ENV['DB_USER'] );

/** MySQL database password */
define( 'DB_PASSWORD', $_ENV['DB_PASS'] );

/** MySQL hostname */
define( 'DB_HOST', $_ENV['DB_HOST'] );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', $_ENV['DB_CHARSET'] );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', $_ENV['DB_COLLATE'] );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', $_ENV['AUTH_KEY'] );
define( 'SECURE_AUTH_KEY', $_ENV['SECURE_AUTH_KEY'] );
define( 'LOGGED_IN_KEY', $_ENV['LOGGED_IN_KEY'] );
define( 'NONCE_KEY', $_ENV['NONCE_KEY'] );
define( 'AUTH_SALT', $_ENV['AUTH_SALT'] );
define( 'SECURE_AUTH_SALT', $_ENV['SECURE_AUTH_SALT'] );
define( 'LOGGED_IN_SALT', $_ENV['LOGGED_IN_SALT'] );
define( 'NONCE_SALT', $_ENV['NONCE_SALT'] );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = $_ENV['TABLE_PREFIX'];

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', $debug );
define( 'WP_DEBUG_LOG', $debug );
define( 'WP_DEBUG_DISPLAY', $debug );
define( 'SCRIPT_DEBUG', $debug );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

if ( ! defined( 'WP_CLI' ) ) {
	/**
	 * Prevent WP Rocket from setting WP_CACHE constant.
	 *
	 * @see https://docs.wp-rocket.me/article/958-how-to-use-wp-rocket-with-bedrock-wordpress-boilerplate
	 */
	add_filter( 'rocket_set_wp_cache_constant', function () {
		return ! isset( $_ENV['WP_CACHE'] ) || $_ENV['WP_CACHE'] === '';
	} );

	/** Auto-updating plugins */
	$auto_update_plugins = ! empty( $_ENV['WP_AUTO_UPDATE_PLUGINS'] ) ? $_ENV['WP_AUTO_UPDATE_PLUGINS'] : false;
	add_filter( 'auto_update_plugin', function () use ( $auto_update_plugins ) {
		return (bool) $auto_update_plugins;
	} );

	/** Auto-updating themes */
	$auto_update_themes = ! empty( $_ENV['WP_AUTO_UPDATE_THEMES'] ) ? $_ENV['WP_AUTO_UPDATE_THEMES'] : false;
	add_filter( 'auto_update_theme', function () use ( $auto_update_themes ) {
		return (bool) $auto_update_themes;
	} );

}